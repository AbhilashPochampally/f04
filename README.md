# 44-564 F04 Working with Python and MapReduce

More practice with Python and MapReduce. 

Copy the new years resolution data from the data folder into a f0input.csv file. 

- Complete 01map.py
- Complete 02sort.py
- Complete 03reduce.py

## Requirements

- Chocolatey, the Windows package manager.
- Python 2.6.6
- Git for Windows
- TortoiseGit
- Add Open Command Window Here as Administrator to your File Explorer context menu.
- Visual Studio Code for text editing
- VS Code Python extension by Microsoft >5M installs)

## References

- https://bitbucket.org/professorcase/h03
https://data.

- https://data.world/crowdflower/2015-new-years-resolutions

- https://data.world/crowdflower/url-categorization

- https://data.world/crowdflower/blockbuster-database